import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { App } from 'app'
import { HomePage } from 'components/home'


export const Routes = () => (
  <App>
    <Switch>
      <Route exact path="/" component={HomePage} />
    </Switch>
  </App>
)

