import { createStore, applyMiddleware, compose } from 'redux'
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk'
import { Api } from 'api'
import { reducer } from 'redux-content'


export const api = new Api('http://dev.frevend.com/')

export function configureStore(rootInitialState = {}) {
  const middlewares = [
    createLogger({ collapsed: true }),
    thunk.withExtraArgument({ api }),
  ]

  // eslint-disable-next-line no-underscore-dangle
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

  return createStore(
    reducer,
    rootInitialState,
    composeEnhancers(applyMiddleware(...middlewares)),
  )
}
