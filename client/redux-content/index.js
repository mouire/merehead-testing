import { combineReducers } from 'redux'
import { usersReducer as users } from './reducer'


export const reducer = combineReducers({
  users,
})
