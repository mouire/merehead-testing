import users from 'data/users.json'
import { LOAD_USERS, APP_LOAD, SET_PAGE } from './types'


export const getUsers = () => ({
  type: LOAD_USERS,
  payload: users,
})

export const mounted = () => ({ type: APP_LOAD })

export const setPage = (page) => ({
  type: SET_PAGE,
  page,
})
