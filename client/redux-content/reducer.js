import {
  APP_LOAD,
  LOAD_USERS,
  SET_PAGE,
} from './types'


const initial = {
  loading: true,
  page: 1,
  users: [],
  total: 0,
  size: 5,
}

export const usersReducer = (state = initial, action) => {
  switch (action.type) {
    case APP_LOAD: {
      if (!localStorage.getItem('page')) localStorage.setItem('page', 1)
      if (!localStorage.getItem('total-pages')) localStorage.setItem('total-pages', 1)
      return {
        ...state,
        page: localStorage.getItem('page'),
        totalPages: localStorage.getItem('total-pages'),
        loading: true,
      }
    }
    case SET_PAGE: {
      const { page } = action

      localStorage.setItem('page', page)

      return {
        ...state,
        page,
        loading: true,
      }
    }
    case LOAD_USERS: {
      const { page, size } = state
      const { users } = action.payload
      const currentUsers = users.slice((page - 1) * size, (page) * size) || []
      const totalPages = Math.ceil(users.length / size)

      localStorage.setItem('total-pages', totalPages)
      return {
        ...state,
        users,
        onCurrentPage: currentUsers,
        loading: false,
        totalPages,
      }
    }

    default:
      return state
  }
}
