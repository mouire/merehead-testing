import * as React from 'react'
import { connect } from 'react-redux'
import { getUsers, mounted } from 'redux-content/actions'
import { lifecycle, compose } from 'recompose'
import PropTypes from 'prop-types'


const AppMount = (props) => (
  <div>
    <header>mouire@icloud.com</header>
    {props.children}
  </div>
)

const mapDispatchToProps = (dispatch) => ({
  app: () => dispatch(mounted()),
  loadUsers: () => dispatch(getUsers()),
})

const enhance = compose(
  connect(null, mapDispatchToProps),
  lifecycle({
    componentDidMount() {
      this.props.app()
      this.props.loadUsers()
    },
  }),
)

AppMount.propTypes = {
  children: PropTypes.node.isRequired,
}
export const App = enhance(AppMount)

