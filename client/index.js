
import React from 'react'
import ReactDom from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { Provider as ReduxProvider } from 'react-redux'
import { Routes } from './routes'
import { configureStore } from './store'
import './style.css'


const rootElement = document.getElementById('root')
const store = configureStore(window.initialStore || {})


const render = () => {
  ReactDom.render(
    (
      <ReduxProvider store={store}>
        <BrowserRouter>
          <Routes />
        </BrowserRouter>
      </ReduxProvider>
    ),
    rootElement,
  )
}

render()

if (module.hot) {
  module.hot.accept(
    ['./routes', './store'],
    render,
  )
}
