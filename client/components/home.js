import React from 'react'
import { UsersList } from 'components/user-list'
import { Pagination } from 'components/pagination'


export const HomePage = () => (
  <div className='content'>
    <div className='home__wrapper'>
      <h2>Users List</h2>
      <Pagination />
      <UsersList />
    </div>
  </div>
)
