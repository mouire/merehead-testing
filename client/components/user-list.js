import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { lifecycle, branch, compose, renderComponent } from 'recompose'
import { getUsers } from 'redux-content/actions'
import { Spinner } from 'components/spinner'
import { UserCard } from 'components/user-card'


const withData = lifecycle({
  componentDidMount() {
    getUsers()
  },
})

const withSpinner = branch(
  (props) => props.loading,
  renderComponent(Spinner),
)
const enhance = compose(
  connect((state) => ({
    loading: state.users.loading,
    users: state.users.onCurrentPage,
  })),
  withData,
  withSpinner,
)

export const UsersList = enhance(({ users }) => (
  <div className='list'>
    {
        users.map((user) => (
          <UserCard key={user.id} user={user} />
        ))
      }
  </div>
))

UsersList.propTypes = {
  users: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.string)),
}
