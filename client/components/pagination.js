import React from 'react'
import { connect } from 'react-redux'
import { withHandlers, compose } from 'recompose'
import PropTypes from 'prop-types'
import { setPage, getUsers } from 'redux-content/actions'


const enhance = compose(
  connect(
    (state) => ({
      page: state.users.page,
      total: state.users.totalPages,
    }),
    (dispatch) => ({
      changePage: (page) => dispatch(setPage(page)),
      loadUsers: () => dispatch(getUsers()),
    }),
  ),
  withHandlers({
    onPage: ({ changePage, loadUsers }) => (page) => {
      changePage(page)
      setTimeout(loadUsers, 1000)
    },
  }),
)

const PaginationView = ({ page, total, onPage }) => {
  const rows = []

  for (let item = 0; item < total; item++) {
    rows.push(item + 1)
  }
  return (
    <div className='pagination'>
      {
        rows.map((p) => (
          <div
            className={`${parseInt(page, 10) === p ? 'active__page' : ''}`}
            role='button'
            onClick={() => onPage(p)}
            key={p}
          >
            {p}
          </div>
        ))
      }
    </div>
  )
}

export const Pagination = enhance(PaginationView)


Pagination.propTypes = {
  page: PropTypes.number,
  total: PropTypes.number,
  onPage: PropTypes.func,
}

Pagination.defaultProps = {
  page: 1,
  total: 1,
  onPage: null,
}
