import React from 'react'


export const Spinner = () => (
  <div className='loading loading__list'>
    <div className='before' />
    <div className='after' />
  </div>
)
