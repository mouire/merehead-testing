import React from 'react'
import PropTypes from 'prop-types'


export const UserCard = ({ user: { id, name, surname, desc } }) => (
  <div className='user__card'>
    <div className='user__photo'>
      <img alt='avatar 'src={`http://dev.frevend.com/json/images/u_${id}.png`} />
    </div>
    <div className='user__info'>
      <h3>{`${name} ${surname}`}</h3>
      <div>{desc}</div>
    </div>
  </div>
)

UserCard.propTypes = {
  user: PropTypes.shape({
    desc: PropTypes.string,
    id: PropTypes.string,
    name: PropTypes.string,
    surname: PropTypes.string,
  }),
}

UserCard.defaultProps = {
  user: {
    desc: null,
    id: '0',
    name: null,
    surname: null,
  },
}
