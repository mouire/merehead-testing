process.title = 'Werehead:webpack'

const { cpus } = require('os')
const { resolve } = require('path')
const {
  optimize: { CommonsChunkPlugin },
  EnvironmentPlugin,
} = require('webpack')
const HappyPack = require('happypack')
const HtmlPlugin = require('html-webpack-plugin')


const { NODE_ENV = 'development' } = process.env
const IS_PROD = NODE_ENV === 'production'
const IS_DEV = NODE_ENV === 'development'
const IS_TEST = NODE_ENV === 'test'

const DIST = resolve(__dirname, '..', 'dist')
const SRC = resolve(__dirname, '..', 'client')

const config = {
  context: SRC,
  target: 'web',

  entry: {
    polyfill: 'babel-polyfill',
    index: ['./index'],
  },

  output: {
    path: DIST,
    publicPath: '/',
  },

  resolve: {
    extensions: ['.mjs', '.js'],
    modules: [
      'node_modules',
      SRC,
    ],
  },

  module: {
    rules: [{
      test: /\.css$/,
      use: ['style-loader', 'css-loader'],
    },
    {
      test: /\.m?js$/,
      exclude: /node_modules/,
      use: [
        'happypack/loader',
      ],
    },
    {
      test: /\.svg$/,
      use: 'react-svg-loader',
    },
    {
      test: /\.(ico|gif|png|jpg|jpeg|webp)$/,
      loaders: ['file-loader'],
      exclude: /node_modules/,
    },
    ],
  },

  plugins: [
    new HappyPack({
      threads: cpus().length,
      loaders: ['babel-loader'],
    }),
    new CommonsChunkPlugin({
      name: 'vendor',
      chunks: ['index'],
      filename: IS_DEV ? '[name].js' : '[name]-[chunkhash].js',
      minChunks: (module) => module.context && module.context.indexOf('node_modules') !== -1,
    }),
    new CommonsChunkPlugin({
      name: 'index',
      filename: IS_DEV ? '[name].js' : '[name]-[chunkhash].js',
      children: true,
      minChunks: 2,
    }),
    new HtmlPlugin({
      title: 'Junior Frontend',
      template: 'index.tpl.html',
    }),
    new EnvironmentPlugin({
      NODE_ENV,
    }),
  ],

  stats: {
    colors: true,
    children: false,
  },
}

module.exports = {
  config,

  IS_DEV,
  IS_PROD,
  IS_TEST,

  DIST,
  SRC,
}
