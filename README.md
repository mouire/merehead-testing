## Test task


[View demo](https://merehead-test-frontend.firebaseapp.com/)


####Tech stack
* HTML5
* CSS
* React/Redux

## Quick start

* Clone this repo using 
`git clone https://mouire@bitbucket.org/mouire/merehead-testing.git`

* Move to the appropriate directory
`cd merehead-testing`

* Run `yarn` or `npm i` to install dependencies

* Run `npm run start` to see the example app at `http://localhost:3000`